import http.server
import socketserver
import subprocess
import threading

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        try:
            if self.path == '/':
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(b'Application is running.')
            else:
                self.send_error(404)
        except Exception as e:
            self.send_response(500)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes('Internal Server Error: {}'.format(e), 'utf-8'))

def start_web_server():
    port = 8080

    with socketserver.TCPServer(("", port), MyHandler) as httpd:
        print("Web server started on port", port)
        httpd.serve_forever()

def start_bot_script():
    try:
        subprocess.run(['python3', '/app/code/bot/main.py'], check=True)
    except subprocess.CalledProcessError as e:
        print("Bot script error:", e)

if __name__ == '__main__':
    web_server_thread = threading.Thread(target=start_web_server)
    web_server_thread.start()

    start_bot_script()
