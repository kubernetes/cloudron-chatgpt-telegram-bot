FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN apt update
#RUN apt upgrade -y
RUN apt install ffmpeg -y

ARG VERSION=0.4.0

RUN mkdir -p /app/code
RUN mkdir -p /app/data/usage_logs

ENV PYTHONFAULTHANDLER=1 \
     PYTHONUNBUFFERED=1 \
     PYTHONDONTWRITEBYTECODE=1 \
     PIP_DISABLE_PIP_VERSION_CHECK=on

RUN curl -L https://github.com/n3d1117/chatgpt-telegram-bot/archive/refs/tags/${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip

RUN mv /tmp/chatgpt-telegram-bot-${VERSION}/* /app/code/
RUN mv /tmp/chatgpt-telegram-bot-${VERSION}/.env.example /app/code/.env.example
WORKDIR /app/code/

RUN cp .env.example /app/data/.env
RUN ln -s /app/data/.env /app/code/.env
RUN ln -s /app/data/usage_logs /app/code/usage_logs

RUN pip install -r requirements.txt --no-cache-dir

EXPOSE 8080

COPY start.sh /app/code/
COPY runner.py /app/code/
CMD [ "/app/code/start.sh" ]
