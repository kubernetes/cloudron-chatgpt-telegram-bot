#!/bin/bash

set -eu

chown -R cloudron:cloudron /app/data
cd /app/code

if [[ ! -f /app/data/.env ]]; then
    echo "==> Copying files on first run"
    cp /app/code/.env.example /app/data/.env
fi

if [[ ! -d /app/data/usage_logs ]]; then
    echo "==> Create usage_logs folder on first run"
    mkdir -p /app/data/usage_logs
fi

echo "==> Starting App"
#exec /usr/local/bin/gosu cloudron:cloudron python3 bot/main.py
exec /usr/local/bin/gosu cloudron:cloudron python3 runner.py
